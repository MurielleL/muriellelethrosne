#' dire_bonjour
#' La fonction permet de dire bonjour au prenom rentre en parametre.
#' @param prenom chaine de caractere
#'
#' @return chaine de caractere
#' @export
#' @importFrom glue glue
#' @examples
#' dire_bonjour(prenom="Murielle")
dire_bonjour<-function(prenom="toi"){
  glue::glue("Bonjour {prenom} !")
}
